using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    [SerializeField] private float m_Speed;
    [SerializeField] private Vector3 m_Direction;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate( m_Direction * m_Speed, Space.Self);
    }
}
