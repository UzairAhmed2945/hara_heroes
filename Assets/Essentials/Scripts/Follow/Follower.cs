using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour
{
    // Start is called before the first frame update
    #region Data
    [SerializeField] private Transform m_Target;
    [SerializeField] private Animator m_Animator;
    private Vector3 m_Initialposition;
    private Quaternion m_InitialRotation;
    #endregion Data

    private void Start()
    {
        m_Initialposition = this.transform.position;
        m_InitialRotation = this.transform.rotation;
    }

    private void OnEnable()
    {
        GameManagerDelegate.OnGameReset += FollowerReset;
        GameManagerDelegate.OnGameComplete += GameComplete;
    }

    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= FollowerReset;
        GameManagerDelegate.OnGameComplete -= GameComplete;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.e_gamestates == eGameStates.Playing)
        {
            transform.LookAt(new Vector3(m_Target.position.x, transform.position.y, m_Target.position.z));
            var distance = Vector2.Distance(transform.position, m_Target.position);
            if (distance > GameConfig.Instance.FollowerStoppingDistance)
            {
                var followerapos = transform.position;
                followerapos.x = Mathf.Lerp(followerapos.x, m_Target.position.x, GameConfig.Instance.FollowerSpeed * Time.deltaTime);
                transform.position = followerapos;
                m_Animator.SetFloat("Blend", 1);
            }
            else
            {
                m_Animator.SetFloat("Blend", 0);
            }
        }
        else if (GameManager.Instance.e_gamestates == eGameStates.Fail)
        {
            m_Animator.SetBool(nameof(eAnimStates.Dead), true);
        }


    }


    public void FollowerReset()
    {
        m_Animator.SetBool(nameof(eAnimStates.Dead), false);
        m_Animator.SetFloat("Blend", 0);
        this.transform.position = m_Initialposition;
        this.transform.rotation = m_InitialRotation;
    }
    public void GameComplete()
    {
        m_Animator.SetBool(nameof(eAnimStates.Dead), false);
        m_Animator.SetFloat("Blend", 0);
    }
}
