using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D.IK;
using UnityEngine.Scripting.APIUpdating;


public class Ikplayer : MonoBehaviour
{
    // Start is called before the first frame update
    public LimbSolver2D Rightlimbsolver;
    public Transform CollectionTarget;
    public Transform NormalTarget;
    public Animator PlayerAnimator;
    public ParticleSystem [] VaccumParticle;
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void MoveToCollectionTargetIk()
    {
        Rightlimbsolver.GetChain(0).target = CollectionTarget;
    }

    public void MoveToNormalTargetIK()
    {
        Rightlimbsolver.GetChain(0).target = NormalTarget;
    }

}

