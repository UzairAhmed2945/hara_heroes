using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConfig : Singleton<GameConfig>
{
    [Header("Player")]
    public float MovementSpeed;
    public float JumpForce;

    [Space]

    [Header("Trash")]
    public float MoveSpeed;
    public float DecreaseSizeSpeed;

    [Space]

    [Header("Camera")]
    public float LeftXLimit;
    public float RightXLimit;
    public float FollowSpeed;


    [Space]

    [Header("Follower Dog")]

    public float FollowerSpeed;
    public float FollowerStoppingDistance;
}
