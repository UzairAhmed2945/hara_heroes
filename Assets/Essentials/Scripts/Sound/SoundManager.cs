using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singleton<SoundManager>
{
    public AudioSource BG;
    public AudioSource SFX;
    public GameObject Water;
    public GameObject VaccumCleaner;
    public GameObject WateringPlants;
    public GameObject Timer;
    public GameObject CitySound;
    public AudioSource WaterAudiosource;
    public AudioSource VaccumCleanerAudiosource;
    public AudioSource TimerAudiosource;
    public AudioSource WateringPlantsAudiosource;
    public AudioSource CitySoundAudiosource;

    [Space]
    [Header("AudioClips")]

    public AudioClip ButtonClickClip;
    public AudioClip MainMEnuMusic;
    public AudioClip GamePlayMusic;
    public AudioClip GameCompleteClip;
    public AudioClip GameFailClip;
    public AudioClip SuctionClip;



    public void ButtonClickSound()
    {
        SFX.clip = ButtonClickClip;
        SFX.Play();
    }

    public void MainMenuSound()
    {
        BG.clip = MainMEnuMusic;
        BG.Play();
    }

    public void GamePlaySound()
    {
        BG.clip = GamePlayMusic;
        BG.Play();
    }

    public void GameCompleteSound()
    {
        SFX.clip = GameCompleteClip;
        SFX.Play();
    }

    public void GameFailSound()
    {
        SFX.clip = GameFailClip;
        SFX.Play();
    }

    public void SuctionSound()
    {
        SFX.clip = SuctionClip;
        SFX.Play();
    }

    public void waterSoundOn()
    {
        if (StorageManager.Instance.CurrentlevelNo == 0)
        {
            Water.SetActive(true);
            CitySound.SetActive(false);
        }
        else
        {
            CitySound.SetActive(true);
            Water.SetActive(false);
        }
    }

    public void waterSoundOFF()
    {
        Water.SetActive(false);
        CitySound.SetActive(false);
    }

    public void VaccumSoundOn()
    {
        if (StorageManager.Instance.CurrentlevelNo == 0)
        {
            VaccumCleaner.SetActive(true);
        }
        else
        {
            WateringPlants.SetActive(true);
        }
    }

    public void VaccumSoundOFF()
    {
        VaccumCleaner.SetActive(false);
        WateringPlants.SetActive(false);
    }

    public void TimerSoundOn()
    {
        Timer.SetActive(true);
    }

    public void TimerSoundOFF()
    {
        Timer.SetActive(false);
    }

    public void SoundOFF()
    {
        BG.mute = true;
        SFX.mute = true;
        WaterAudiosource.mute = true;
        VaccumCleanerAudiosource.mute =true;
        TimerAudiosource.mute =true;
        WateringPlantsAudiosource.mute = true;
        CitySoundAudiosource.mute = true;
    }

    public void SoundOn()
    {
        BG.mute = false;
        SFX.mute = false;
        WaterAudiosource.mute = false;
        VaccumCleanerAudiosource.mute = false;
        TimerAudiosource.mute = false;
        WateringPlantsAudiosource.mute = false;
        CitySoundAudiosource.mute = false;
    }

   

}
