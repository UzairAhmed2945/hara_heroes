﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;

public class ExtendedButton : Button,IDragHandler, IBeginDragHandler, IEndDragHandler,IPointerDownHandler,IPointerUpHandler
{
    public Action onDown = () => { };
    public Action onUp = () => { };
    public Action<PointerEventData> OnDownEvent = (i_PointerEventData) => { };
    public Action<PointerEventData> OnUpEvent = (i_PointerEventData) => { };
    public Action<PointerEventData> OnBeginDragEvent = (i_PointerEventData) => { };
    public Action<PointerEventData> OnDragEvent = (i_PointerEventDataevData) => { };
    public Action<PointerEventData> OnEndDragEvent = (i_PointerEventData) => { };
    

    #region Events
    public override void OnPointerDown(PointerEventData i_PointerEventData)
    {
        base.OnPointerDown(i_PointerEventData);

        OnDownEvent.Invoke(i_PointerEventData);
        onDown.Invoke();
    }

    public override void OnPointerUp(PointerEventData i_PointerEventData)
    {
        base.OnPointerUp(i_PointerEventData);

        OnUpEvent.Invoke(i_PointerEventData);
        onUp.Invoke();
    }

    public virtual void OnBeginDrag(PointerEventData i_PointerEventData)
    {
        OnBeginDragEvent(i_PointerEventData);
    }

    public virtual void OnDrag(PointerEventData i_PointerEventData)
    {
        OnDragEvent(i_PointerEventData);
    }

    public virtual void OnEndDrag(PointerEventData i_PointerEventData)
    {
        OnEndDragEvent(i_PointerEventData);
    }
    #endregion
}
