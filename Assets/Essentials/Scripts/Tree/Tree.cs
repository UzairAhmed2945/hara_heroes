using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour
{
    #region Data

    [SerializeField] private float m_TreeGrowingSpeed;
    [SerializeField] private float m_TreeShrinkSpeed;

    [SerializeField] private Transform m_Tree;
    [SerializeField] private float m_MaxScale;

    private bool m_IsWatering;
    private bool m_IsDead = true;
    private bool m_HasGrown;
    [SerializeField] private GameObject m_DirectionalArrow;
    [SerializeField] private Animator m_SmileyFace;

    #endregion Data
    private void OnEnable()
    {
        GameManagerDelegate.OnGameReset += Reset;
    }

    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= Reset;
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            m_IsWatering = false;
        }

        if (m_IsWatering)
        {
            if (m_Tree.transform.localScale.x < m_MaxScale)
                m_Tree.transform.localScale += Vector3.one * m_TreeGrowingSpeed * Time.deltaTime;


            if (m_Tree.transform.localScale.x >= m_MaxScale && !m_HasGrown)
            {
                TreeFullGrown();
                m_HasGrown = true;
            }
                m_IsDead = false;
            
            
        }
        else if(!m_IsDead && !m_HasGrown)
        {
            if (m_Tree.transform.localScale.x > 0 )
            {
                m_Tree.transform.localScale -= Vector3.one * m_TreeShrinkSpeed * Time.deltaTime;
            }
            else
            {
                m_IsDead = true;
                //TreeDied();
            }

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(nameof(eTagType.Collector)) && Input.GetMouseButton(0))
        {
            m_IsWatering = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag(nameof(eTagType.Collector)))
        {
            m_IsWatering = false;
        }
    }

    private void TreeFullGrown()
    {

        LevelManager.Instance.TaskDoneCount++;
        HUDManager.Instance.TaskDoneCount.text = LevelManager.Instance.TaskDoneCount.ToString();

        HUDManager.Instance.SetTaskCollected(LevelManager.Instance.TaskDoneCount, LevelManager.Instance.TotalTaskToDo);

        if (LevelManager.Instance.TaskDoneCount == LevelManager.Instance.TotalTaskToDo)
        {
            GameManager.Instance.GameComplete();
        }
        m_DirectionalArrow.SetActive(false);
        m_SmileyFace.Rebind();
        m_SmileyFace.gameObject.SetActive(true);
    }

    private void TreeDied()
    {
        LevelManager.Instance.TaskDoneCount--;
        HUDManager.Instance.TaskDoneCount.text = LevelManager.Instance.TaskDoneCount.ToString();

        HUDManager.Instance.SetTaskCollected(LevelManager.Instance.TaskDoneCount, LevelManager.Instance.TotalTaskToDo);
    }

    private void Reset()
    {
        m_Tree.transform.localScale = Vector2.zero;
        m_IsDead = true;
        m_HasGrown = false;
        m_DirectionalArrow.SetActive(true);
        m_SmileyFace.gameObject.SetActive(false);
    }
}
