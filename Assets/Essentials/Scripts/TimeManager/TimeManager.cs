using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : Singleton<TimeManager>
{
    public float targetTime;
    private float m_StartTime;

    void Update()
    {
        if (GameManager.Instance.e_gamestates == eGameStates.Playing) {
            SoundManager.Instance.TimerSoundOn();
            targetTime -= Time.deltaTime;
            HUDManager.Instance.SetRemainingTime(GameManager.Instance.Remaping(targetTime, m_StartTime, 0, 0, 1));

            if (targetTime <= 0.0f)
            {
                timerEnded();
            }
        }
        else
        {
            SoundManager.Instance.TimerSoundOFF();
        }

    }

    void timerEnded()
    {
        GameManager.Instance.GameFail();
    }

    public void setTargetTime(float StartingTime)
    {
        m_StartTime = StartingTime;
        targetTime = StartingTime;
    }
}
